# Acer4738G-HighSierra-OpenCore0.5.5

重点：
1. 解决AMD 6370M显卡Vbios的载入及设备ID仿冒问题。
2. 解决ACPI中没有网卡设备地址及仿冒设备ID问题。
3. 解决机器发热量大问题。
4. 解决睡眠唤醒问题。
5. 不兼容Windows系统。




